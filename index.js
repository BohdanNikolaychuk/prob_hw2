const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

const { notesRouter } = require('./router/notesRouter');
const { usersRouter } = require('./router/usersRouter');
const { infoRouter } = require('./router/infoRouter');
mongoose.connect(
  'mongodb+srv://admin:admin123@cluster0.tcu5vyi.mongodb.net/test?retryWrites=true&w=majority',
);
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/users', infoRouter);
app.use('/api/auth', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

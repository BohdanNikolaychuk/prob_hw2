const express = require('express');

const router = express.Router();
const { registerUser, loginUser } = require('../service/usersService.js');

router.post('/register', registerUser);
router.post('/login', loginUser);

module.exports = {
  usersRouter: router,
};

const express = require('express');

const router = express.Router();
const {
  getUserNotes,
  createUserNote,
  getNote,
  editNote,
  editNoteValue,
  deleteUserNoteById,
} = require('../service/notesService.js');

const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getUserNotes);
router.post('/', authMiddleware, createUserNote);
router.get('/:id', authMiddleware, getNote);
router.put('/:id', authMiddleware, editNote);
router.patch('/:id', authMiddleware, editNoteValue);
router.delete('/:id', authMiddleware, deleteUserNoteById);

module.exports = {
  notesRouter: router,
};
